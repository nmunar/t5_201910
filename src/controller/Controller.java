package controller;

import java.awt.List;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.Collections;
import java.util.Iterator;

import com.opencsv.CSVReader;

import model.data_structures.*;
import model.util.Sort;
import model.vo.LocationVO;
import model.vo.VOMovingViolation;
import view.MovingViolationsManagerView;

@SuppressWarnings("unused")
public class Controller {

	private MovingViolationsManagerView view;

	// DONE Definir las estructuras de datos para cargar las infracciones del periodo definido
	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private IQueue<VOMovingViolation> movingViolationsQueue;

	/**
	 * Cola de prioridad
	 */
	private IMaxColaPrioridad<LocationVO> locationViolationsMaxPQ;


	/**
	 * Heap de prioridad
	 */
	private IMaxHeapCP<LocationVO> heapMaxCP;

	// Muestra obtenida de los datos cargados 
	private VOMovingViolation [ ] muestra;

	// Copia de la muestra de datos a ordenar 
	private VOMovingViolation[ ] muestraCopia;

	//Arreglo de los elementos cargados

	private VOMovingViolation[] arreglo = new VOMovingViolation[373543];

	boolean carga = false;

	String objectId = "";

	String location = "";

	String ticketIssueDate ="";

	String totalPaid ="";

	String accidentIndicator ="";

	String violationDescription ="";

	String adressId ="";

	int sumaFINEAMT =0;

	public Controller() {
		view = new MovingViolationsManagerView();
		//DONE inicializar las estructuras de datos para la carga de informacion de archivos
		movingViolationsQueue = new Queue<VOMovingViolation>();

		locationViolationsMaxPQ = new MaxColaPrioridad<LocationVO>();

		heapMaxCP =new MaxHeapCP<LocationVO>();
	}

	/**
	 * Leer los datos de las infracciones de los archivos. Cada infraccion debe ser Comparable para ser usada en los ordenamientos.
	 * Todas infracciones (MovingViolation) deben almacenarse en una Estructura de Datos (en el mismo orden como estan los archivos)
	 * A partir de estos datos se obtendran muestras para evaluar los algoritmos de ordenamiento
	 * @return numero de infracciones leidas 
	 */
	public int loadMovingViolations() {
		// TODO Los datos de los archivos deben guardarse en la Estructura de Datos definida

		int cont = 0;
		carga = true;
		try {

			String archCSV = "./data/Moving_Violations_Issued_in_January_2018.csv";


			for(int i = 0; i < 4; i++) {
				CSVReader csvReader = new CSVReader(new FileReader(archCSV));
				String[] fila = null;
				csvReader.readNext();
				while((fila = csvReader.readNext()) != null) {

					objectId = fila[0];
					location = fila[2];
					ticketIssueDate = fila[13];
					sumaFINEAMT = Integer.parseInt(fila[8]);
					totalPaid = fila[9];
					accidentIndicator = fila[12];
					violationDescription = fila[15];
					adressId = fila[3];

					if(adressId.equals("")) {

						adressId = "0";

					}
					//Se crea el objeto VOMovingViolations

					VOMovingViolation infraccion = new VOMovingViolation(objectId, location, ticketIssueDate, totalPaid, accidentIndicator, violationDescription, adressId);
					movingViolationsQueue.enqueue(infraccion);
					arreglo[cont] = infraccion;

					cont++;

				}
				csvReader.close();
				if(i==0)
				{
					archCSV = "./data/Moving_Violations_Issued_in_February_2018.csv";
				}else if(i ==1)
				{
					archCSV = "./data/Moving_Violations_Issued_in_March_2018.csv";
				} else if(i == 2) {

					archCSV = "./data/Moving_Violations_Issued_in_April_2018.csv";

				}


			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return cont;
	}



	/**
	 * Agrega los datos a la maxPriorityQueue
	 */

	public int agregarMPQyHeap(int caso) {		

		locationViolationsMaxPQ = new MaxColaPrioridad<LocationVO>();
		heapMaxCP = new MaxHeapCP<LocationVO>();

		int i = 0;


		while(i  < muestra.length-1) {

			int numberOfRegistersVO = 0;
			int adressIdVO = muestra[i].getAdressId();
			String locationVo = muestra[i].getLocation();

			while(  i < muestra.length && adressIdVO == muestra[i].getAdressId()) {

				numberOfRegistersVO++;

				i++;
			}

			LocationVO nuevoLocationVO = new LocationVO(adressIdVO, locationVo, numberOfRegistersVO);


			if(caso == 0) {

				//SE AGREGA EL ELEMENTO A LA MAX PRIORITY QUEUE
				locationViolationsMaxPQ.agregar(nuevoLocationVO);

				//SE AGREGA EL ELEMENTO AL HEAP
				heapMaxCP.agregar(nuevoLocationVO);

			} else if(caso == 1) {

				//SE AGREGA EL ELEMENTO A LA MAX PRIORITY QUEUE
				locationViolationsMaxPQ.agregar(nuevoLocationVO);

			} else if(caso == 2) {

				//SE AGREGA EL ELEMENTO AL HEAP
				heapMaxCP.agregar(nuevoLocationVO);

			}

		}

		/*		Iterator<LocationVO> iter = locationViolationsMaxPQ.iterator();

				while(iter.hasNext()) {		

					LocationVO actual = iter.next();

					System.out.println(actual.getNumberOfRegisters()+"-" + actual.getAdressId());



			}*/


		return locationViolationsMaxPQ.darNumElementos();

	}

	/**
	 * Mide el tiempo que se tarda en agregar los datos a una Max Priority Queue
	 */

	public long getTimeAddingMaxPQ() {

		long startTime;
		long endTime;
		long duration;

		startTime = System.currentTimeMillis();
		agregarMPQyHeap(1);
		endTime = System.currentTimeMillis();

		duration = endTime - startTime;

		return duration;
	}

	/**
	 * Mide el tiempo en que se tarda en agregar los datos a un MaxHeap
	 */

	public long getTimeAddingMaxHeap() {

		long startTime;
		long endTime;
		long duration;

		startTime = System.currentTimeMillis();
		agregarMPQyHeap(2);
		endTime = System.currentTimeMillis();

		duration = endTime - startTime;

		return duration;
	}


	/**
	 * Mide el tiempo en que se tarda en eliminar el dato de mayor prioridad en un MaxPQ
	 */

	public long getTimeDeletingMaxPQ() {

		long startTime;
		long endTime;
		long duration;

		startTime = System.currentTimeMillis();

		while(locationViolationsMaxPQ.darNumElementos() > 0) {

			locationViolationsMaxPQ.delMax();

		}
		endTime = System.currentTimeMillis();

		duration = endTime - startTime;

		return duration;
	}


	/**
	 * Mide el tiempo en que se tarda en eliminar el dato de mayor prioridad en un MaxHeap
	 */

	public long getTimeDeletingMaxHeap() {

		long startTime;
		long endTime;
		long duration;

		startTime = System.currentTimeMillis();
		while(heapMaxCP.darNumElementos() > 0) {

			heapMaxCP.delMax();

		}
		endTime = System.currentTimeMillis();

		duration = endTime - startTime;

		return duration;
	}


	/**
	 * Generar una muestra aleatoria de tamaNo n de los datos leidos.
	 * Los datos de la muestra se obtienen de las infracciones guardadas en la Estructura de Datos.
	 * @param n tamaNo de la muestra, n > 0
	 * @return muestra generada
	 */
	public void generarMuestra ( int n )
	{
		muestra = new VOMovingViolation[ n ];			
		// TODO Llenar la muestra aleatoria con los datos guardados en la estructura de datos

		int cont = 0;

		while(cont < n) {

			int randomInt = (int) (Math.random()* arreglo.length);

			muestra[cont] = arreglo[randomInt];

			cont++;
		}

	}

	/**
	 * Generar una copia de una muestra. Se genera un nuevo arreglo con los mismos elementos.
	 * @param muestra - datos de la muestra original
	 * @return copia de la muestra
	 */
	public VOMovingViolation [ ] obtenerCopia( VOMovingViolation [ ] muestra)
	{
		VOMovingViolation[ ] copia = new  VOMovingViolation[ muestra.length ]; 
		for ( int i = 0; i < muestra.length; i++)
		{    copia[i] = muestra[i];    }
		return copia;
	}

	/**
	 * Ordenar datos aplicando el algoritmo Mergesort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public void ordenarMergeSort( VOMovingViolation[ ] datos ) {

		VOMovingViolation[] aux = new VOMovingViolation[datos.length];

		Sort.ordenarMergeSort(datos, aux, 0, datos.length-1);
	}


	/**
	 * Invertir una muestra de datos (in place).
	 * datos[0] y datos[N-1] se intercambian, datos[1] y datos[N-2] se intercambian, datos[2] y datos[N-3] se intercambian, ...
	 * @param datos - conjunto de datos a invertir (inicio) y conjunto de datos invertidos (final)
	 */
	public void invertirMuestra( VOMovingViolation[ ] datos ) {
		// TODO implementar

		VOMovingViolation datosAux[] = new VOMovingViolation[muestra.length];

		int pos = 0;
		for(int i = muestra.length-1; i >= 0; i--) {

			datosAux[pos] = datos[i];

			pos++;
		}

		muestra = datosAux;


	}

	public void viasMasImportantes()
	{
		while(!heapMaxCP.esVacia())
		{
			System.out.println(heapMaxCP.delMax().getLocation());
		}
	}

	public void cantidadPorADID()
	{Iterator<LocationVO> lo= locationViolationsMaxPQ.iterator();
	lo.next();
	while(lo.hasNext())
	{
		LocationVO dat = lo.next();
		System.out.println(dat.getAdressId()+"\t \t  "+dat.getNumberOfRegisters());
	}
	}

	/**
	 * M�todo run
	 */
	public void run() {
		long startTime;
		long endTime;
		long duration;

		int nDatos = 0;
		int nMuestra = 0;

		Scanner sc = new Scanner(System.in);
		boolean fin = false;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:
				// Cargar infracciones

				if(carga == true) {

					view.printMensage("Los datos ya se hab�an cargado.");

				} else {
					nDatos = this.loadMovingViolations();
					view.printMensage("Numero infracciones cargadas:" + nDatos);
				}
				break;

			case 2:
				// Generar muestra de infracciones a ordenar
				if(carga == true) {
					view.printMensage("Dar tamaNo de la muestra: ");
					nMuestra = sc.nextInt();
					generarMuestra( nMuestra );
					ordenarMergeSort(muestra);
					agregarMPQyHeap(0);
					view.printMensage("Muestra generada");
					break;
				} else {

					view.printMensage("Debe primero cargar los datos.");

				}

				break;
			case 3:
				// Mostrar los datos de la muestra actual (original)
				if ( nMuestra > 0 && muestra != null && muestra.length == nMuestra )
				{    
					view.printDatosMuestra( nMuestra, muestra);
				}
				else
				{
					view.printMensage("Muestra invalida");
				}
				break;


			case 4:

				if(carga == true) {	

					view.printMensage("Cantidad de elementos en el MaxPQ: " + locationViolationsMaxPQ.darNumElementos()+"\n" +"Cantidad de elementos en el MaxHeapCP: " + heapMaxCP.darNumElementos());

				} else {

					view.printMensage("Debe primero cargar los datos.");

				}

				break;

			case 5:

				if(carga == true && muestra != null) {

					long time = getTimeAddingMaxPQ();

					float numerador = time;
					float tiempo = time;

					float division = numerador / tiempo;
					view.printMensage("Agregar los LocationVO a un MaxPQ dur�: " + time + " milisegundos.");
					view.printMensage("El promedio de la operaci�n agregar fue de: " + (division) + " milisegundos.");

				} else {

					view.printMensage("Debe primero cargar los datos o generar una muestra.");

				}

				break;

			case 6:

				if(carga == true && muestra != null) {

					long time = getTimeAddingMaxHeap();

					float numerador = time;
					float tiempo = time;

					float division = numerador / tiempo;

					view.printMensage("Agregar los LocationVO a un MaxHeap dur�: " + time + " milisegundos.");
					view.printMensage("El promedio de la operaci�n agregar fue de: " + (division) + " milisegundos.");


				} else {

					view.printMensage("Debe primero cargar los datos o generar una muestra.");

				}

				break;


			case 7:

				if(carga == true && muestra != null) {

					long time = getTimeDeletingMaxPQ();

					float numerador = time;
					float tiempo = time;

					float division = numerador / tiempo;

					view.printMensage("Eliminar el elemento de mayor prioridad en una MaxPQ fue de: " + time + " milisegundos.");
					view.printMensage("El promedio de la operaci�n eliminar fue de: " + (division) + " milisegundos.");


				} else {

					view.printMensage("Debe primero cargar los datos o generar una muestra.");

				}

				break;
			case 8:

				if(carga == true && muestra != null) {

					long time = getTimeDeletingMaxHeap();

					float numerador = time;
					float tiempo = time;

					float division = numerador / tiempo;

					view.printMensage("Eliminar el elemento de mayor prioridad en un MaxHeap fue de: " + time + " milisegundos.");
					view.printMensage("El promedio de la operaci�n eliminar fue de: " + (division) + " milisegundos.");


				} else {

					view.printMensage("Debe primero cargar los datos o generar una muestra.");

				}
				break;

			case 9:

				if(carga == true && muestra != null) {

					view.printMensage("Las vias mas importantes son de menor a mayor son:");
					viasMasImportantes();

				} else {

					view.printMensage("Debe primero cargar los datos o generar una muestra.");

				}
				break;
			case 10:

				if(carga == true && muestra != null) {

					view.printMensage("AdressId"+"\t"+"Cant");
					cantidadPorADID();				
				} else {

					view.printMensage("Debe primero cargar los datos o generar una muestra.");

				}

				break;
			case 11:	
				fin=true;
				sc.close();
				break;
			}
		}
	}

}
