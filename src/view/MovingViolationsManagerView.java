package view;

public class MovingViolationsManagerView 
{
	public MovingViolationsManagerView() {

	}

	public void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 4----------------------");
		System.out.println("1. Cargar datos de infracciones en movimiento");
		System.out.println("2. Obtener una muestra de datos a ordenar");
		System.out.println("3. Mostrar la muestra de datos a ordenar");
		System.out.println("4. Cantidad de LocationVO");
		System.out.println("5. Tiempo en agregar los LocationVO en una MaxPQ y promedio de la operaci�n agregar");
		System.out.println("6. Tiempo en agregar los LocationVO en una MaxHeap y promedio de la operaci�n agregar");
		System.out.println("7. Tiempo en eliminar el elemento de mayor prioridad en una MaxPQ");
		System.out.println("8. Tiempo en eliminar el elemento de mayor prioridad en un MaxHeap");
		System.out.println("9. Mostrar las vias m�s importantes por su numero de accidentes (ordenadas de mayor a menor)");
		System.out.println("10. Mostrar cantidad de infracciones con el mismo AdressId");
		System.out.println("11. Salir");
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}

	public void printDatosMuestra( int nMuestra, Comparable [ ] muestra)
	{
		for ( Comparable dato : muestra)
		{	System.out.println(  dato.toString() );    }
	}

	public void printMensage(String mensaje) {
		System.out.println(mensaje);
	}
}
