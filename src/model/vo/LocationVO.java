package model.vo;

public class LocationVO implements Comparable<LocationVO>{
		
	/**
	 * Atributos
	 */
	
	private int adressId = -1; 
	
	private String location = "";
	
	private int numberOfRegisters =-1;
	
	
	public LocationVO(Integer pAdressId, String pLocation, Integer pNumberOfRegisters) {
		
		adressId = pAdressId;
		
		location = pLocation;
		
		numberOfRegisters = pNumberOfRegisters;
		
	}
	
	
	public int getAdressId() {
		
		return adressId;
		
	}
	
	public String getLocation() {
		
		return location;
		
	}

	public int getNumberOfRegisters() {
		
		return numberOfRegisters;
		
	}
	@Override
	public int compareTo(LocationVO o) {
		int deter = 90;
		if(o.getNumberOfRegisters() == numberOfRegisters)
		{
			if(o.getLocation().equals(location))
			{
				deter = 0;
			}else if(o.getLocation().compareTo(location) < 0)
			{
				deter = 1;
			}else if(o.getLocation().compareTo(location) > 0)
			{
				deter = -1;
			}

		}else if(o.getNumberOfRegisters() < numberOfRegisters)
		{
			deter = 1;
		}else if(o.getNumberOfRegisters() > numberOfRegisters)
		{
			deter = -1;
		}
		return deter;
	}

}
