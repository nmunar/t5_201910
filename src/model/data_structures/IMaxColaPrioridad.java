package model.data_structures;

public interface IMaxColaPrioridad<T> extends Iterable<T>{

	/**
	 * Retorna n�mero de elementos presentes en la cola de prioridad
	 * @return cantidad de elementos
	 */
	public int darNumElementos();


	/**
	 * Agrega un elemento a la cola. Si el elemento ya existe y tiene una prioridad
	 *diferente, el elemento debe actualizarse en la cola de prioridad.
	 */
	public void agregar(T elemento);


	/**
	 * Saca/atiende el elemento m�ximo en la cola y lo retorna; null en caso de
	 *cola vac�a
	 * @return elemento que se elimin�
	 */
	public T delMax();


	/**
	 * Obtener el elemento m�ximo (sin sacarlo de la Cola); null en caso de cola
	 *vac�a
	 * @return el elemento con m�xima prioridad de la cola
	 */
	public T max();

	/**
	 * Retorna si la cola est� vac�a o no
	 * @return true si est� vac�a, false de lo contrario
	 */
	public boolean esVacia();


}
