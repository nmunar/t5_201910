package model.data_structures;

import java.util.Iterator;

import model.vo.LocationVO;

public class MaxColaPrioridad <T extends Comparable<T>> implements IMaxColaPrioridad<T>{

	/**
	 * Atributos
	 */

	private int longitud;

	private Node<T> primero;

	private Node<T>  ultimo; 

	private Node<T>  actual;


	public MaxColaPrioridad() {
		// TODO Auto-generated constructor stub

		longitud = 0;

		primero = null;

	}

	/**
	 * Retorna n�mero de elementos presentes en la cola de prioridad
	 * @return cantidad de elementos
	 */
	@Override
	public int darNumElementos() {
		return longitud;
	}


	/**
	 * Agrega un elemento a la cola. Si el elemento ya existe y tiene una prioridad
	 *diferente, el elemento debe actualizarse en la cola de prioridad.
	 */
	@Override
	public void agregar(T elemento) {

		Node<T> agregado = new Node<T>((T) elemento);


		if(primero == null) {

			longitud++;
			primero = agregado;
			ultimo = agregado;
			actual = agregado;

		} else {

			if(agregado.darElemento().compareTo(primero.darElemento()) > 0) {


				longitud++;
				agregado.cambiarSiguiente(primero);
				primero.cambiarAnterior(agregado);
				primero = agregado;
				actual = agregado;

			} else {


				if(actual.darSiguiente() != null && (agregado.darElemento().compareTo(actual.darElemento()) <= 0) && agregado.darElemento().compareTo(actual.darSiguiente().darElemento()) >= 0) {

					agregado.cambiarSiguiente(actual.darSiguiente());
					actual.darSiguiente().cambiarAnterior(agregado);

					longitud++;

					agregado.cambiarAnterior(actual);
					actual.cambiarSiguiente(agregado);
					actual = primero;

				} else if(actual.darSiguiente() == null && (agregado.darElemento().compareTo(actual.darElemento()) <= 0)) {

					longitud++;
					actual.cambiarSiguiente(agregado);
					agregado.cambiarAnterior(actual);
					ultimo = agregado;
					actual = primero;

				} else {

					actual = actual.darSiguiente();
					agregar(elemento);

				}

			}

		}	

	}

	/**
	 * Saca/atiende el elemento m�ximo en la cola y lo retorna; null en caso de
	 *cola vac�a
	 * @return elemento que se elimin�
	 */
	@Override
	public T delMax() {
		// TODO Auto-generated method stub

		T elemento = null;

		if(!esVacia()) {

			elemento = primero.darElemento();

			if(primero.darSiguiente() != null) {
				primero.darSiguiente().cambiarAnterior(null);
			}
			primero = primero.darSiguiente();

			longitud --;

		}

		return elemento;
	}


	/**
	 * Obtener el elemento m�ximo (sin sacarlo de la Cola); null en caso de cola
	 *vac�a
	 * @return el elemento con m�xima prioridad de la cola
	 */
	@Override
	public T max() {
		// TODO Auto-generated method stub

		T maxPrioridad = null;

		if(!esVacia()) {
			maxPrioridad = primero.darElemento();
		}

		return maxPrioridad;

	}


	/**
	 * Retorna si la cola est� vac�a o no
	 * @return true si est� vac�a, false de lo contrario
	 */
	@Override
	public boolean esVacia() {
		// TODO Auto-generated method stub
		return longitud == 0;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new IteratorSQ<>(primero);
	}

}
