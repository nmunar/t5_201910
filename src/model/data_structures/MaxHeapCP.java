package model.data_structures;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
/**
 * Arreglo de objetos de tipo T
 * @author Munar
 * @author Algorithms, 4th Edition by Robert Sedgewick and Kevin Wayne
 * @param <T>
 */
public class MaxHeapCP<T extends Comparable<T>> implements IMaxHeapCP<T>
{
	private int ocupadas;
	private T[] heap;
	private Comparator<T> comparator;  // optional comparator

	/**
	 * Arreglo de elementos de tamaNo maximo
	 */
	public MaxHeapCP(int maxN) 
	{
		heap = (T[])new Comparable[maxN + 1];
		ocupadas = 0;
	} 

	/**
	 * Inicializa un heap .
	 */
	public MaxHeapCP() 
	{
		this(1);
	}

	/**
	 * Initializes an empty priority queue with the given initial capacity,
	 * using the given comparator.
	 *
	 * @param  initCapacity the initial capacity of this priority queue
	 * @param  comparator the order in which to compare the keys
	 */
	public MaxHeapCP(int initCapacity, Comparator<T> comparator) {
		this.comparator = comparator;
		heap = (T[]) new Object[initCapacity + 1];
		ocupadas = 0;
	}

	/**
	 * Initializes an empty priority queue using the given comparator.
	 * @param  comparator the order in which to compare the keys
	 */
	public MaxHeapCP(Comparator<T> comparator) {
		this(1, comparator);
	}

	/**
	 * Initializes a priority queue from the array of keys.
	 * Takes time proportional to the number of keys, using sink-based heap construction.
	 *
	 * @param  keys the array of keys
	 */
	public MaxHeapCP(T[] keys) {
		ocupadas = keys.length;
		heap = (T[]) new Comparable[keys.length + 1];
		for (int i = 0; i < ocupadas; i++)
			heap[i+1] = keys[i];
		for (int k = ocupadas/2; k >= 1; k--)
			sink(k);
		assert isMaxHeap();

	}

	@Override
	public void agregar( T dato )
	{
		// double size of array if necessary
		if (ocupadas == heap.length - 1) 
			resize(2 * heap.length);

		// add dato, and percolate it up to maintain heap invariant
		heap[++ocupadas] = dato;
		swim(ocupadas);
		assert isMaxHeap();
	}

	@Override
	public int darNumElementos() 
	{
		return ocupadas;
	}

	/**
	 * Saca/atiende el elemento m�ximo en la cola y lo retorna; null en caso de cola vac�a
	 */
	@Override
	public T delMax ()
	{
		if (esVacia()) 
			throw new NoSuchElementException("No hay elementos en el heap");
		T max = heap[1];
		exch(1, ocupadas--);
		sink(1);
		heap[ocupadas+1] = null;     // to avoid loiteing and help with garbage collection
		if ((ocupadas > 0) && (ocupadas == (heap.length - 1) / 4)) resize(heap.length / 2);
		assert isMaxHeap();
		return max;
	}

	private void resize(int capacity) {
		assert capacity > ocupadas;
		T[] temp = (T[]) new Comparable[capacity];
		for (int i = 1; i <= ocupadas; i++) {
			temp[i] = heap[i];
		}
		heap = temp;
	}

	/**
	 * Obtener el elemento m�ximo (sin sacarlo de la Cola); nullen caso de cola vac�a
	 */
	@Override
	public T max ()
	{
		if (esVacia()) 
			throw new NoSuchElementException("Priority queue underflow");
		return heap[1];
	}

	/**
	 * Retorna si la cola est� vac�a o no
	 */
	@Override
	public boolean esVacia()
	{
		return ocupadas ==0;
	}

	/***************************************************************************
	 * Helper functions for compares and swaps.
	 ***************************************************************************/
	private boolean less(int i, int j) 
	{
		if (comparator == null) {
			return ((Comparable<T>)heap[i]).compareTo(heap[j]) < 0;
		}
		else {
			return comparator.compare(heap[i], heap[j]) < 0;
		}
	}

	private void exch(int i, int j) {
		T swap = heap[i];
		heap[i] = heap[j];
		heap[j] = swap;
	}
	/***************************************************************************
	 * Heap helper functions.
	 ***************************************************************************/

	private void swim(int k) 
	{
		while (k > 1 && less(k/2, k)) {
			exch(k, k/2);
			k = k/2;
		}
	}

	private void sink(int k) {
		while (2*k <= ocupadas) {
			int j = 2*k;
			if (j < ocupadas && less(j, j+1)) j++;
			if (!less(k, j)) break;
			exch(k, j);
			k = j;
		}
	}

	private boolean isMaxHeap() {
		return isMaxHeap(1);
	}
	private boolean isMaxHeap(int k)
	{
		if (k > ocupadas) return true;
		int left = 2*k;
		int right = 2*k + 1;
		if (left  <= ocupadas && less(k, left))  return false;
		if (right <= ocupadas && less(k, right)) return false;
		return isMaxHeap(left) && isMaxHeap(right);
	}

}
